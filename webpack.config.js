const webpack = require('webpack');
const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: './index.web.ts',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.js|jsx|ts|tsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-react', '@babel/preset-env'],
          plugins: [
            ['react-native-web', {commonjs: true}],
            '@babel/plugin-syntax-jsx',
          ],
        },
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  devServer: {
    static: {
      directory: path.join(__dirname, './web'),
    },
    compress: true,
    port: 9000,
  },
  resolve: {
    alias: {
      'react-native': 'react-native-web',
    },
    extensions: ['.tsx', '.ts', '.js', 'jsx'],
  },
};
