import {AppRegistry} from 'react-native';
import App from './App';

AppRegistry.registerComponent('tinak', () => App);
AppRegistry.runApplication('tinak', {
  rootTag: document.getElementById('react-native-app'),
});
