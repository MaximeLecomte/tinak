import React from 'react';
import {HelloBanner} from './libraries/HelloBanner';
import {HomeScreen} from './src/features/HomeScreen';

const App = () => {
  return (
    <HomeScreen>
      <HelloBanner />
    </HomeScreen>
  );
};

export default App;
