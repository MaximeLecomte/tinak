import {Platform, SafeAreaView} from 'react-native';
import React, {ReactNode} from 'react';

export const HomeScreen = (props: React.PropsWithChildren<ReactNode>) => {
  return (
    <SafeAreaView
      style={{
        backgroundColor: 'steelblue',
        minHeight: Platform.OS === 'web' ? '100vh' : '100%',
      }}>
      {props.children}
    </SafeAreaView>
  );
};
