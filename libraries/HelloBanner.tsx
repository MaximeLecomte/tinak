import {Text} from 'react-native';
import React from 'react';

export const HelloBanner = () => {
  return (
    <>
      <Text
        style={{
          color: 'white',
          fontSize: 50,
          textAlign: 'center',
          fontWeight: 'bold',
        }}>
        Hello Tinak
      </Text>
      <Text
        style={{color: 'lightsteelblue', fontSize: 30, textAlign: 'center'}}>
        Ready to fly?!
      </Text>
    </>
  );
};
